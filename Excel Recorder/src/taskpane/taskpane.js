//----------------------------------------------------------------------
// CONSTANTS
//----------------------------------------------------------------------

// Event IDs
const contextSwitchEventID = 1;
const mouseEventID = 3; 
const selectEventID = 6;
const sortEventID = 8;
const textInputEventID = 9;
const textSelectEventID = 10;
const findEventID = 11;

//----------------------------------------------------------------------
// EVENT SUBSCRIPTION
//----------------------------------------------------------------------
Office.onReady(info => {
  if (info.host === Office.HostType.Excel) {
    // Determine if the user's version of Office supports all the Office.js APIs that are used in the tutorial.
    if (!Office.context.requirements.isSetSupported('ExcelApi', '1.7')) {
      console.log('Sorry. The tutorial add-in uses Excel.js APIs that are not available in your version of Office.');
    }

    // Call all subscription methods here
    onCreated();
    subscribeSheetsToOnActivated();
    subscribeSheetsToOnSingleClicked();
    subscribeSheetsToOnColumnSorted();
    subscribeSheetsToOnRowSorted();
    subscribeSheetsToOnSelectionChanged()
  }
});

export async function onCreated() {
  try {
    await Excel.run(async context => {
      var sheet = context.workbook.worksheets.getActiveWorksheet();
      sheet.load("name");

      await context.sync();

      // Create Context Switch Event here
      let contextSwitchEvent = new ContextSwitchEvent(sheet.name, 'Opened a new workbook', getTime());
      let contextSwitchEventObj = contextSwitchEvent.asJSONObject();

      // Sending the event goes here
      send(contextSwitchEventObj);
    });
  } catch (error) {
    console.error(error);
  }
}

export async function subscribeSheetsToOnActivated() {
  try {
    await Excel.run(async context => {
      var sheets = context.workbook.worksheets;
      sheets.load("items/name");

      await context.sync();

      for (var i in sheets.items) {
        // Subscribe to the onActviated event of each sheet
        sheets.items[i].onActivated.add(function (event) {
            onActivatedResponse(event);
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
}

export async function subscribeSheetsToOnSingleClicked() {
  try {
    await Excel.run(async context => {
      var sheets = context.workbook.worksheets;
      sheets.load("items/name");

      await context.sync();

      for (var i in sheets.items) {
        // Subscribe to the onActviated event of each sheet
        sheets.items[i].onSingleClicked.add(function (event) {
          onMouseEventResponse(event);
          onSelectEventResponse(event);
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
}

export async function subscribeSheetsToOnColumnSorted() {
  try {
    await Excel.run(async context => {
      var sheets = context.workbook.worksheets;
      sheets.load("items/name");

      await context.sync();

      for (var i in sheets.items) {
        // Subscribe to the onActviated event of each sheet
        sheets.items[i].onColumnSorted.add(function (event) {
          // Response goes here
          onSortEventResponse(event, 'column');
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
}

export async function subscribeSheetsToOnRowSorted() {
  try {
    await Excel.run(async context => {
      var sheets = context.workbook.worksheets;
      sheets.load("items/name");

      await context.sync();

      for (var i in sheets.items) {
        // Subscribe to the onActviated event of each sheet
        sheets.items[i].onRowSorted.add(function (event) {
          // Response goes here
          onSortEventResponse(event, 'row');
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
}

export async function subscribeSheetsToOnSelectionChanged() {
  try {
    await Excel.run(async context => {
      var sheets = context.workbook.worksheets;
      sheets.load("items/name");

      await context.sync();

      for (var i in sheets.items) {
        // Subscribe to the onActviated event of each sheet
        sheets.items[i].onSelectionChanged.add(function (event) {
          // Response goes here
          onTextSelectResponse(event);
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
}

//----------------------------------------------------------------------
// EVENT RESPONSES
//----------------------------------------------------------------------

export async function onActivatedResponse(event) {
  try {
    await Excel.run(async context => {
      let worksheet = context.workbook.worksheets.getItem(event.worksheetId);
      worksheet.load("name");

      await context.sync();

      // Create Context Switch here
      // Create Context Switch Event here
      let contextSwitchEvent = new ContextSwitchEvent(worksheet.name, 'switched the current worksheet', getTime());
      let contextSwitchEventObj = contextSwitchEvent.asJSONObject();

      // Sending the event goes here
      getScreenshotAndSend(contextSwitchEventObj);
    });
  } catch (error) {
    console.error(error);
  }
}

export async function onMouseEventResponse(event) {
  try {
    await Excel.run(async context => {
      await context.sync();

      let mouseEvent = new MouseEvent(event.offsetX, event.offsetY, 'left click', getTime());
      let mouseEventObj = mouseEvent.asJSONObject();

      getScreenshotAndSend(mouseEventObj);
    });
  } catch (error) {
    console.error(error);
  }
}

export async function onSelectEventResponse(event) {
  try {
    await Excel.run(async context => {
      await context.sync();

      let selectEvent = new SelectEvent(event.address, getTime());
      let selectEventObj = selectEvent.asJSONObject();

      getScreenshotAndSend(selectEventObj);
    });
  } catch (error) {
    console.error(error);
  }
}

export async function onSortEventResponse(event, type) {
  try {
    await Excel.run(async context => {
      await context.sync();

      let sortEvent = new SortEvent(event.address, type, getTime());
      let sortEventObj = sortEvent.asJSONObject();

      getScreenshotAndSend(sortEventObj);
    });
  } catch (error) {
    console.error(error);
  }
}

export async function onTextSelectResponse(event) {
  try {
    await Excel.run(async context => {
      let sheet = context.workbook.worksheets.getActiveWorksheet();
      let range = sheet.getRange(event.address);
      range.load("text");

      await context.sync();

      let textSelectEvent = new TextSelectEvent(etsets(range.text), getTime());
      let textSelectEventObj = textSelectEvent.asJSONObject();  

      getScreenshotAndSend(textSelectEventObj);
    });
  } catch (error) {
    console.error(error);
  }
}

/*export async function run() {
  try {
    await Excel.run(async context => {

    });
  } catch (error) {
    console.error(error);
  }
}*/


//----------------------------------------------------------------------
// EVENT MODELS
//----------------------------------------------------------------------

/**
 * This is the context switch event model
 */
function ContextSwitchEvent(context, description, time) {
  this.context = context;
  this.description = description;
  this.time = time;

  /**
   * Converts this object into a json object
   * 
   * @returns {contextSwitchEventObj} The ContextSwitchEvent as JSON Object
   */
  this.asJSONObject = function() {
      //time = BackgroundUtility.getTime();
      let contextSwitchEvent = '{"eventID" : "' + contextSwitchEventID + '", "newContext" : "' + context + '", "description" : "' + description + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
      let contextSwitchEventObj = JSON.parse(contextSwitchEvent);

      return contextSwitchEventObj;
  }

  /**
   * Converts this object into a json object
   * 
   * @returns {contextSwitchEventObj} The ContextSwitchEvent as JSON Object
   */
  this.asJSONObjectWithScreenshot = function(screenshot) {
      //time = BackgroundUtility.getTime();
      let contextSwitchEvent = '{"eventID" : "' + contextSwitchEventID + '", "newContext" : "' + context + '", "description" : "' + description + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
      let contextSwitchEventObj = JSON.parse(contextSwitchEvent);

      return contextSwitchEventObj;
  }
}

/**
 * This is the Mouse event prototype
 * */ 
function MouseEvent(x, y, clickType, time) {
  this.x = x;
  this.y = y;
  this.clickType = clickType;
  this.time = time;

  /**
   * Converts this object into a json object
   * 
   * @returns {mouseClickEventObj} The MouseEvent as JSON Object
   */
  this.asJSONObject = function() {
      //time = ContentUtility.getTime();
      let mouseClickEvent = '{"eventID" : "' + mouseEventID + '", "xCoordinate" : "' + Math.floor(x) + '", "yCoordinate" : "' + Math.floor(y) + '", "clickType" : "' + clickType + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
      let mouseClickEventObj = JSON.parse(mouseClickEvent);

      return mouseClickEventObj;
  }

  /**
   * Converts this object into a json object
   * 
   * @returns {mouseClickEventObj} The MouseEvent as JSON Object
   */
  this.asJSONObjectWithScreenshot = function(screenshot) {
      //time = ContentUtility.getTime();
      let mouseClickEvent = '{"eventID" : "' + mouseEventID + '", "xCoordinate" : "' + Math.floor(x) + '", "yCoordinate" : "' + y + '", "clickType" : "' + clickType + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
      let mouseClickEventObj = JSON.parse(mouseClickEvent);

      return mouseClickEventObj;
  }
}

/**
 * This is the Select event prototype
 * */ 
function SelectEvent(cell, time) {
  this.cell = cell;
  this.time = time;

  /**
   * Converts this object into a json object
   * 
   * @returns {ccpEventObj} The CCPEvent as JSON Object
   */
  this.asJSONObject = function() {
      let selectEvent = '{"eventID" : "' + selectEventID + '", "data" : "' + cell + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
      let selectEventObj = JSON.parse(selectEvent);

      return selectEventObj;
  }

  /**
   * Converts this object into a json object
   * 
   * @returns {ccpEventObj} The CCPEvent as JSON Object
   */
  this.asJSONObjectWithScreenshot = function(screenshot) {
      let selectEvent = '{"eventID" : "' + selectEventID + '", "data" : "' + cell + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
      let selectEventObj = JSON.parse(selectEvent);

      return selectEventObj;
  }
}

/**
 * This is the Select event prototype
 * */ 
function SortEvent(range, type, time) {
  this.range = range;
  this.type = type;
  this.time = time;

  /**
   * Converts this object into a json object
   * 
   * @returns {ccpEventObj} The CCPEvent as JSON Object
   */
  this.asJSONObject = function() {
      let sortEvent = '{"eventID" : "' + sortEventID + '", "range" : "' + range + '", "type" : "' + type + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
      let sortEventObj = JSON.parse(sortEvent);

      return sortEventObj;
  }

  /**
   * Converts this object into a json object
   * 
   * @returns {ccpEventObj} The CCPEvent as JSON Object
   */
  this.asJSONObjectWithScreenshot = function(screenshot) {
    let sortEvent = '{"eventID" : "' + sortEventID + '", "range" : "' + range + '", "type" : "' + type + '", "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
    let sortEventObj = JSON.parse(sortEvent);

    return sortEventObj;
  }
}

/**
 * This is the Text Select event prototype
 * */ 
function TextSelectEvent(text, time) {
  this.text = text;
  this.time = time;

  /**
   * Converts this object into a json object
   * 
   * @returns {ccpEventObj} The CCPEvent as JSON Object
   */
  this.asJSONObject = function() {
      let textSelectEvent = '{"eventID" : "' + textSelectEventID + '", "text" : "' + text + '", "screenshot" : "' + 'NONE' + '", "time" : "' + time + '"}';
      let textSelectEventObj = JSON.parse(textSelectEvent);

      return textSelectEventObj;
  }

  /**
   * Converts this object into a json object
   * 
   * @returns {ccpEventObj} The CCPEvent as JSON Object
   */
  this.asJSONObjectWithScreenshot = function(screenshot) {
      let textSelectEvent = '{"eventID" : "' + textSelectEventID + '", "text" : ' + text + ', "screenshot" : "' + screenshot + '", "time" : "' + time + '"}';
      let textSelectEventObj = JSON.parse(textSelectEvent);

      return textSelectEventObj;
  }
}

//----------------------------------------------------------------------
// UTILITY
//----------------------------------------------------------------------

function send(eventData, screenshot) {
  // Event Sendig goes here
  // Sending and receiving data in JSON format using POST method
  let xhr = new XMLHttpRequest();
  let url = "http://localhost:15578/api/postEvent/excel/" + eventData.eventID;
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-Type", "application/json");
  eventData.screenshot = screenshot;
  xhr.send(JSON.stringify(eventData));
}

function getScreenshotAndSend(eventData) {
  send(eventData, "");
}

function getTime () {
  let date = new Date();

  let time =  date.getHours() + ":" +  
              date.getMinutes() + ":" +
              date.getSeconds();

  return time;
}

function etsets(array) {
  let s = '';

  for (var i = 0; i < array.length; i++) {
    var a = array[i];
    for (var j = 0; j < a.length; j++) {
      s += a[j];
      if(j !== a.length - 1)
        s += " | ";
    }

    if (i !== array.length - 1)
      s += "屌";
  }

  return s;
}